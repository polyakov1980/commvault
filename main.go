package main

import (
  "strings"
  "net/http"
  "io/ioutil"
  "os"
  "github.com/tidwall/gjson"
  "encoding/json"
  "time"
  "regexp"
  "path/filepath"
  "strconv"
  "log"
  "fmt"
  "github.com/go-telegram-bot-api/telegram-bot-api"
  "github.com/glenn-brown/golang-pkg-pcre/src/pkg/pcre"
  "encoding/base64"
)
type Configuration struct {
    Clientname string
    Urlstart   string
    Password   string
    Username   string
    TelegramToken string
    TelegramChatID int64
}
//log output to log
var (
  outfile, _ = os.Create("/tmp/commvault_upload_"+strconv.Itoa(int(time.Now().Year()))+strconv.Itoa(int(time.Now().Month()))+strconv.Itoa(int(time.Now().Day()))+"_"+strconv.Itoa(int(time.Now().Hour()))+strconv.Itoa(int(time.Now().Minute()))+".log") // update path for your needs
  l      = log.New(outfile, "", 0)
)
//main function
func main() {
  config:=getConfig("commvault/conf.json")
  bsname := `BS_PostgresSQL`
  clientname := config.Clientname
  urlstart := config.Urlstart
  path := "/mnt/pgsql-full-m1" //where file looking for
  basepath := path+`/commvault/`
  r := ".*tar.gz.*" //which files are finding
  subDirSkip := []string{"backup_tmp","commvault"} //which dirs skiping from find
  //create nested map in map
  //  14 months              re="0106|0112" #First day of half year
  //  6 months              re1="01" #First day of month
  //  1 months              re2="7" #Sanday
  //  1 months    else other 
  m := map[string]map[string]string{
  "RKT_DL_LTO6_24month":map[string]string{},
  "RKT_DL_LTO6_14month":map[string]string{},
  "RKT_DL_LTO6_6month":map[string]string{},
  "RKT_DL_LTO6_1month":map[string]string{},
  "RKT_DL_LTO6_1week":map[string]string{},
  }
  m["RKT_DL_LTO6_24month"]["DL_LTO7_007days_M1_LTO7_030days_DL_LTO7_5years_encryption_Rocket"] = "^(0106|0112)"
  m["RKT_DL_LTO6_6month"]["DL_LTO7_6month_M1_LTO7_6month_encryption"] = "^01$"
  m["RKT_DL_LTO6_1month"]["DL_LTO7_1month_M1_LTO7_1month_encryption"] = "^7$"
  m["RKT_DL_LTO6_1week"]["DL_LTO7_1month_M1_LTO7_1month_encryption"] = "^\\d{2,2}(?<![0][1])(?![7]\\b)\\d{1,1}\\b"
//^\d{2,2}(?<![0][1])(?![7]\b)\d{1,1}\b
///
//gmU
//^ asserts position at start of a line
//\d{2,2} matches a digit (equal to [0-9])
//{2,2} Quantifier — Matches exactly 2 times
//Negative Lookbehind (?<![0][1])
//Assert that the Regex below does not match
//Match a single character present in the list below [0]
//0 matches the character 0 literally (case sensitive)
//Match a single character present in the list below [1]
//1 matches the character 1 literally (case sensitive)
//Negative Lookahead (?![7]\b)
//Assert that the Regex below does not match
//Match a single character present in the list below [7]
//7 matches the character 7 literally (case sensitive)
//\b assert position at a word boundary: (^\w|\w$|\W\w|\w\W)
//\d{1,1} matches a digit (equal to [0-9])
//{1,1} Quantifier — Matches exactly one time (meaningless quantifier)
//\b assert position at a word boundary: (^\w|\w$|\W\w|\w\W)
//Global pattern flags
//g modifier: global. All matches (don't return after first match)
//m modifier: multi line. Causes ^ and $ to match the begin/end of each line (not only begin/end of string)
//U modifier: Ungreedy. The match becomes lazy by default. Now a ? following a quantifier makes it greedy

  //Get token
  tokenstring := gettoken(urlstart,config)
  //after get token then get clients
  name2 := getclientid(urlstart,tokenstring,clientname)
  l.Println(`Clientid: `+name2)
  //after get clientid, then get backupset
  bs := getbackupsetid(urlstart,name2,bsname,tokenstring)
  l.Println(`Backup set ID: `+bs)
  //after get backup set id, then get property of backup set id
  psprop := getbsprop(urlstart,tokenstring,bs)
  l.Println(`Backup set content path: `+psprop)
//for every policy make own job
for _policy,_map := range m {
for _storagePolicy,_regexp := range _map {
  var new_files []string
  l.Printf("_policy[%s],_storagePolicy[%s],_regexp[%s]\n", _policy,_storagePolicy,_regexp)
  _, files := findFiles(path,r,_regexp,subDirSkip)
  //move files into dirs
  //check policy dir is empty befor move
  _dsize,_:=DirSize(path+"/commvault/"+_policy)
  _dsizeStr:=ByteCountDecimal(_dsize)
  if _dsize != 0 {
	  l.Println(`Policy: `+_policy+` directory not empty = `+_dsizeStr+`TB`)
	  break
  }
  for _, r := range files {
    //prevent empty string
    if len(r)==0 {
	    continue
    }
    _, file := filepath.Split(r)
    mtime, err := statTimes(r)
    if err != nil {
        l.Println(err)
        return
    }
    //take only files more then 7 days ago
    t := time.Now()
    t_weekago := t.AddDate(0, 0, -7)
    _dsize,_:=DirSize(path+"/commvault/"+_policy)
    _dsizeTb := _dsize/1024/1024/1024/1024
    if t_weekago.After(mtime) && _dsizeTb < 1 { //t_weekago newer then mtime or mtime older then t_weekago
      l.Println("moveFile("+path+"/commvault/"+_policy+"/"+file+", "+r+")")
      moveFile(path+"/commvault/"+_policy+"/"+file, r)
      //create new array with matched files
      new_files = append(new_files, path+"/commvault/"+_policy+"/"+file)
    }
  }
  //after get backup set properties, then subclient id
  subclientid := getsubclientid(urlstart,tokenstring,name2,_policy)
  l.Println(`Subclient ID: `+ subclientid+", Policy: "+_policy)
  // check if subclient empty then create new one
  if len(subclientid) == 0 {
    //create new sublient with path and policy
    new_subclientid := createSubclient(urlstart,tokenstring,bsname,_policy,clientname,basepath+_policy,_storagePolicy)
    subclientid = new_subclientid
    l.Println(`Create Subclient ID: `+ new_subclientid)
  }
  //after create subclient or get subclientid then create task with this subclient
  _dsize,_=DirSize(path+"/commvault/"+_policy)
  //_dsizeStr:=strconv.FormatInt(_dsize,10)
  _dsizeStr=ByteCountDecimal(_dsize)
  //check alredy running backup
  jobRunning := GetJobs(name2,"Active","backup",tokenstring,urlstart)
  countJobs := gjson.Get(jobRunning, `jobs.#(jobSummary.subclientName=="`+_policy+`")#|#`)
  l.Println("Count jobs "+countJobs.String()+" in policy "+_policy)
  //if len(countJobs.String()) == 0 || countJobs.Int() > 0 {
  if len(countJobs.String()) != 0 && countJobs.Int() > 0 {
    break
  }
  //check size of dir after move
  if _dsize != 0 {
  jobid := createTask(urlstart,tokenstring,name2,bs,subclientid)
  l.Println(`JobId: `+jobid)

  //after git jobid, waite while it finish
  //TODO use regexp for not only sucsess result. prevent loop when failed
  times := 0
  re_binary,_:=pcre.Compile("(Runn|Wait|Pend)ing|Suspended",0)
  for done := "";done!="Completed";{
    time.Sleep(300000 * time.Millisecond)
    times = times+1
    done = jobstatus(urlstart,jobid,tokenstring);
    match:=re_binary.MatcherString(done,0).Matches()
    if times > 4 && match==false {
      //temporary asked Donatas
      //killJob(urlstart,tokenstring,jobid)
      break
    }
  }
  //check again status of job and if Completed then delete files from dir
  jobstatus := jobstatus(urlstart,jobid,tokenstring)
  l.Println("Status Job "+jobid+": "+jobstatus )
  sendTelegram("Dir size: "+_dsizeStr+", Time: "+time.Now().String()+", Job: "+jobid+", Status: "+jobstatus+", Policy: "+_policy,config)
  if jobstatus == "Completed" {
    l.Println(new_files)
    for _, new_r := range new_files {
      l.Println(new_r)
      deleteFile(new_r)
    }
  }
  }

//this two brackets close loops for map
}
}
//this last bracket func main
}
func jobstatus(_url,_jobid,_token string) string {
  url := _url+"Job/"+_jobid
  method := "GET"
  client := &http.Client {
  }
  req, err := http.NewRequest(method, url, nil)
  if err != nil {
    l.Println(err)
  }
  req.Header.Add("Accept", "application/json")
  req.Header.Add("Authtoken", _token)
  req.Header.Add("Cookie", "JSESSIONID=16AE0A134219243630EFE87ACD0CFA0D; JSESSIONID=6BC29D3FC9E657DE7EEAE13F4A844F93")
  res, err := client.Do(req)
  defer res.Body.Close()
  body, err := ioutil.ReadAll(res.Body)
  return gjson.Get(string(body), `jobs.0.jobSummary.status|@pretty:{"sortKeys":true}`).String()
}
func gettoken(_url string, _config Configuration) string {
  url := _url+"Login"
  method := "POST"
  encodedPassword := base64.StdEncoding.EncodeToString([]byte(_config.Password))
  payload := strings.NewReader("{\n  \"password\": \""+encodedPassword+"\",\n  \"username\": \""+_config.Username+"\"\n}")
  client := &http.Client {
  }
  req, err := http.NewRequest(method, url, payload)
  if err != nil {
    l.Println(err)
  }
  req.Header.Add("Accept", "application/json")
  req.Header.Add("Content-Type", "application/json")
  res, err := client.Do(req)
  if err != nil {
    l.Println(err)
  }
  defer res.Body.Close()
  body, err := ioutil.ReadAll(res.Body)
  if err != nil {
    l.Println(err)
  }
  bodystring := string(body)
  token := gjson.Get(bodystring,"token")
  //println(`Token: `+token.String())
  return token.String()
}
func getclientid(_url,_token,_clientname string) string {
  url2 := _url+"Client"
  method2 := "GET"
  client2 := &http.Client {
  }
  req2, err := http.NewRequest(method2, url2, nil)
  if err != nil {
    l.Println(err)
  }
  req2.Header.Add("Accept", "application/json")
  req2.Header.Add("Authtoken", _token)
  res2, err := client2.Do(req2)
  if err != nil {
    l.Println(err)
  }
  defer res2.Body.Close()
  body2, err := ioutil.ReadAll(res2.Body)
  if err != nil {
    l.Println(err)
  }
  clientid :=  gjson.Get(string(body2), `clientProperties.#(client.clientEntity.clientName="`+_clientname+`").client.clientEntity.clientId`)
  return clientid.String()
}
func getbackupsetid(_url,_clientid,_bsname,_token string) string {
  url3 := _url+"Backupset?clientId="+_clientid
  method3 := "GET"
  client3 := &http.Client {
  }
  req3, err := http.NewRequest(method3, url3, nil)
  if err != nil {
    l.Println(err)
  }
  req3.Header.Add("Accept", "application/json")
  req3.Header.Add("Authtoken", _token)
  res3, err := client3.Do(req3)
  if err != nil {
    l.Println(err)
  }
  defer res3.Body.Close()
  body3, err := ioutil.ReadAll(res3.Body)
  if err != nil {
    l.Println(err)
  }
  bs := gjson.Get(string(body3), `backupsetProperties.#(backupSetEntity.backupsetName="`+_bsname+`").backupSetEntity.backupsetId`)

  return bs.String()
}
func getbsprop(_url,_token,_bs string) string {
  url4 := _url+"Backupset/"+_bs
  method4 := "GET"
  client4 := &http.Client {
  }
  req4, err := http.NewRequest(method4, url4, nil)
  if err != nil {
    l.Println(err)
  }
  req4.Header.Add("Accept", "application/json")
  req4.Header.Add("Authtoken", _token)
  res4, err := client4.Do(req4)
  if err != nil {
    l.Println(err)
  }
  defer res4.Body.Close()
  body4, err := ioutil.ReadAll(res4.Body)
  if err != nil {
    l.Println(err)
  }
  psprop := gjson.Get(string(body4), `backupsetProperties.#(backupSetEntity.backupsetId="`+_bs+`").content|@pretty:{"sortKeys":true}`)
  return psprop.String()
}
func getsubclientid(_url,_token,_clientid,_policy string) string {
  url5 := _url+"Subclient?clientId="+_clientid
  method5 := "GET"
  client5 := &http.Client {
  }
  req5, err := http.NewRequest(method5, url5, nil)
  if err != nil {
    l.Println(err)
  }
  req5.Header.Add("Accept", "application/json")
  req5.Header.Add("Authtoken", _token)
  req5.Header.Add("Cookie", "JSESSIONID=0DE0B3E82D68FB244D58759DE30B74D2; JSESSIONID=6BC29D3FC9E657DE7EEAE13F4A844F93")
  res5, err := client5.Do(req5)
  defer res5.Body.Close()
  body5, err := ioutil.ReadAll(res5.Body)
  subclientid := gjson.Get(string(body5), `subClientProperties.#(subClientEntity.subclientName="`+_policy+`").subClientEntity.subclientId|@pretty:{"sortKeys":true}`)
  return subclientid.String()
}
func createSubclient(_url,_token,_bsname,_policy,_clientname,_path,_storage_policy string) string {
  url6 := _url+"Subclient"
  method6 := "POST"
  payload6 := strings.NewReader(`{
"subClientProperties": 
 {
 "subClientEntity": 
   {
   "clientName": "`+_clientname+`",
   "appName": "File System",
   "backupsetName": "`+_bsname+`",
   "subclientName": "`+_policy+`"
   },
 "contentOperationType": "ADD",
 "content": [
   {
   "path": "`+_path+`"
   }
 ],
 "commonProperties": {
   "storageDevice": {
     "dataBackupStoragePolicy": {
       "storagePolicyName": "`+_storage_policy+`"
     }
   }
  }
 }
}`)
  client6 := &http.Client {
  }
  req6, err := http.NewRequest(method6, url6, payload6)
  if err != nil {
    l.Println(err)
  }
  req6.Header.Add("Accept", "application/json")
  req6.Header.Add("Authtoken", _token)
  req6.Header.Add("Content-Type", "application/json")
  req6.Header.Add("Cookie", "JSESSIONID=0DE0B3E82D68FB244D58759DE30B74D2; JSESSIONID=6BC29D3FC9E657DE7EEAE13F4A844F93")
  res6, err := client6.Do(req6)
  defer res6.Body.Close()
  body6, err := ioutil.ReadAll(res6.Body)
  if err != nil {
    l.Println(err)
  }
  subclientid2 := gjson.Get(string(body6), `response.entity.subclientId|@pretty:{"sortKeys":true}`)
  return subclientid2.String()
}
func createTask(_url,_token,_clientid,_bs,_subclientid string) string {
  _type:=``
  weekday := time.Now().Weekday()
  _weekday := int(weekday)
  if _weekday == 7 {
    _type = `FULL`
  } else {
    _type = `FULL`
    //_type := `INCREMENTAL`
  }
  payload7 := strings.NewReader(`{
  "taskInfo": {
    "associations": [
      {
        "applicationId": 29,
        "clientId": `+_clientid+`,
        "commCellId": 0,
        "backupsetId": `+_bs+`,
        "_type_": 7,
        "appName": "File System",
        "subclientId": `+_subclientid+`
      }
    ],
    "task": {
      "sequenceNumber": 10,
      "initiatedFrom": 2,
      "taskType": 1,
      "policyType": 0,
      "taskId": 0,
      "taskFlags": {
        "disabled": false
      }
    },
    "subTasks": [
      {
        "subTask": {
          "subTaskType": 2,
          "operationType": 2
        },
        "options": {
          "backupOpts": {
            "truncateLogsOnSource": false,
            "sybaseSkipFullafterLogBkp": false,
            "backupLevel": "`+_type+`",
            "runIncrementalBackup": true,
            "isSpHasInLineCopy": false,
            "runSILOBackup": false,
            "doNotTruncateLog": false,
            "dataOpt": {
              "skipCatalogPhaseForSnapBackup": false,
              "followMountPoints": true,
              "enableIndexCheckPointing": false,
              "enforceTransactionLogUsage": false,
              "spaceReclamation": false,
              "skipConsistencyCheck": false,
              "createNewIndex": false,
              "verifySynthFull": false
            },
            "dataPathOpt": {
              "mediaAgent": {
                "mediaAgentId": 0
              },
              "library": {
                "libraryId": 0
              },
              "drivePool": {
                "drivePoolId": 0
              },
              "spareGroup": {
                "spareMediaGroupId": 0
              },
              "drive": {
                "driveId": 0
              }
            },
            "mediaOpt": {
              "markMediaFullOnSuccess": false,
              "numberofDays": 30,
              "startNewMedia": true,
              "retentionJobType": 2,
              "allowOtherSchedulesToUseMediaSet": true,
              "reserveResourcesBeforeScan": false
            }
          },
          "commonOpts": {
            "jobDescription": "",
            "jobRetryOpts": {
              "killRunningJobWhenTotalRunningTimeExpires": false,
              "numberOfRetries": 0,
              "enableNumberOfRetries": false,
              "runningTime": {
                "enableTotalRunningTime": false,
                "totalRunningTime": 3600
              }
            },
            "startUpOpts": {
              "startInSuspendedState": false,
              "useDefaultPriority": true,
              "priority": 166
            }
          }
        }
      }
    ]
  }
}`)
  url7 := _url+"CreateTask"
  method7 := "POST"
  client7 := &http.Client {
  }
  req7, err := http.NewRequest(method7, url7, payload7)
  if err != nil {
    l.Println(err)
  }
  req7.Header.Add("Accept", "application/json")
  req7.Header.Add("Authtoken", _token)
  req7.Header.Add("Content-Type", "application/json")
  req7.Header.Add("Cookie", "JSESSIONID=B6384ADBC68414D1EA0955DB404932D9; JSESSIONID=6BC29D3FC9E657DE7EEAE13F4A844F93")
  res7, err := client7.Do(req7)
  if err != nil {
    l.Println(err)
  }
  defer res7.Body.Close()
  body7, err := ioutil.ReadAll(res7.Body)
  if err != nil {
    l.Println(err)
  }
  //l.Println(string(body7))
  jobid := gjson.Get(string(body7), `jobIds.0|@pretty:{"sortKeys":true}`)
  return jobid.String()
}
func GetJobs(_clientid,_category,_filter,_token,_url string) string {
  url := _url + "/Job?completedJobLookupTime=300&clientId="+_clientid+"&jobCategory="+_category+"&jobFilter="+_filter
  method := "GET"

  client := &http.Client {
  }
  req, err := http.NewRequest(method, url, nil)

  if err != nil {
    l.Println(err)
  }
  req.Header.Add("Accept", "application/json")
  req.Header.Add("Authtoken", _token)
  req.Header.Add("Cookie", "JSESSIONID=91178D459B02DC4E7C29163C747A34D6")

  res, err := client.Do(req)
  defer res.Body.Close()
  body, err := ioutil.ReadAll(res.Body)
  return string(body)
}
func killJob(_url,_token,_jobid string) string {

  url := _url+"Job/"+_jobid+"/action/kill"
  method := "POST"

  payload := strings.NewReader("")

  client := &http.Client {
  }
  req, err := http.NewRequest(method, url, payload)

  if err != nil {
    l.Println(err)
  }
  req.Header.Add("Accept", "application/json")
  req.Header.Add("Authtoken", _token)
  req.Header.Add("Cookie", "JSESSIONID=1F0509E15B6FB96E4C3D3BBB7A9778DB; JSESSIONID=6BC29D3FC9E657DE7EEAE13F4A844F93")

  res, err := client.Do(req)
  defer res.Body.Close()
  body, err := ioutil.ReadAll(res.Body)

  return string(body)
}
func findFiles(_path, _r, _re string, _subDirsSkip []string) (error,[]string) {
  var matrix []string
  libRegEx, e := regexp.Compile(_r)
  if e != nil {
          log.Fatal(e)
  }
  _RegEx, err := pcre.Compile(_re,0)
  if err != nil {
          log.Fatal(e)
  }
  e = filepath.Walk(_path, func(path string, info os.FileInfo, err error) error {
          if err != nil {
                  l.Printf("prevent panic by handling failure accessing a path %q: %v\n", path, err)
                  //return err
                  return err
          }
          for _, _subDirSkip := range _subDirsSkip {
                  if info.IsDir() && info.Name() == _subDirSkip {
                          //l.Printf("skipping a dir without errors: %+v \n", path + " , FileName: " +info.Name())
                          return filepath.SkipDir
                  }
          }
          if err == nil && libRegEx.MatchString(info.Name()) {
          //split path to get date
          delim := regexp.MustCompile("[-./]")
          s := delim.Split(info.Name(),-1)
          _yearInt,_ := strconv.Atoi(s[2])
          _monthInt,_ := strconv.Atoi(s[3])
          _dayInt,_ := strconv.Atoi(s[4])
          _, _month, _day, _week := s[2], s[3], s[4], time.Date(_yearInt,time.Month(_monthInt),_dayInt,0,0,0,0,time.UTC).Weekday()
          _week222 := int(_week)
          if err == nil && libRegEx.MatchString(info.Name()) && (_RegEx.MatcherString(_day+_month,0).Matches() || _RegEx.MatcherString(_day+strconv.Itoa(_week222),0).Matches() || _RegEx.MatcherString(_day,0).Matches() || _RegEx.MatcherString(strconv.Itoa(_week222),0).Matches()) {
                  matrix = append(matrix, path)
          }}
  return nil
  })
  if e != nil {
          log.Fatal(e)
  }
  return nil,matrix
}
func moveFile(_src,_dest string) error {
  err := os.Rename(_dest, _src)
  if err != nil {
    log.Fatal(err)
  }
  return err
}
func deleteFile(_dest string) error {
  err := os.Remove(_dest)
  if err != nil {
    l.Println("error:", err)
  }
  return err
}
func getConfig(_file string) Configuration {
  file, _ := os.Open(_file)
  defer file.Close()
  decoder := json.NewDecoder(file)
  configuration := Configuration{}
  err := decoder.Decode(&configuration)
  if err != nil {
    l.Println("error:", err)
  }
  return configuration
}
func statTimes(name string) (mtime time.Time, err error) {
    fi, err := os.Stat(name)
    if err != nil {
        return
    }
    mtime = fi.ModTime()
    return
}
func sendTelegram(Text string,_config Configuration) {
  bot, err := tgbotapi.NewBotAPI(_config.TelegramToken)
        if err != nil {
                log.Panic(err)
        }
        bot.Debug = true
        msg:=tgbotapi.NewMessage(_config.TelegramChatID,Text)
        bot.Send(msg)
}
func DirSize(path string) (int64, error) {
	var size int64
	err := filepath.Walk(path, func(_ string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if !info.IsDir() {
			size += info.Size()
		}
		return err
	})
	return size, err
}
func ByteCountDecimal(b int64) string {
        const unit = 1000
        if b < unit {
                return fmt.Sprintf("%d B", b)
        }
        div, exp := int64(unit), 0
        for n := b / unit; n >= unit; n /= unit {
                div *= unit
                exp++
        }
        return fmt.Sprintf("%.1f %cB", float64(b)/float64(div), "kMGTPE"[exp])
}
